package ru.svshubin.transfer.slow;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.jetty.server.Server;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.svshubin.transfer.Main;
import ru.svshubin.transfer.model.Account;
import ru.svshubin.transfer.model.request.account.CreateAccountRequest;
import ru.svshubin.transfer.model.request.account.GetAccountRequest;
import ru.svshubin.transfer.model.request.billing.DepositRequest;
import ru.svshubin.transfer.model.request.billing.TransferRequest;
import ru.svshubin.transfer.model.request.billing.WithdrawRequest;
import ru.svshubin.transfer.resource.interceptor.WrappedResponse;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.Assert.*;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public class ResourcesTest {
    private static final int JETTY_PORT = 8079;
    private static final int ADMIN_MONEY_AMOUNT = 100;
    private static org.h2.tools.Server h2Server;
    private static Server jettyServer;
    private static ObjectMapper objectMapper;

    @BeforeClass
    public static void init() throws Exception {
        h2Server = Main.startH2Database();
        jettyServer = Main.startJetty(JETTY_PORT);
        postJson("/account/create", new CreateAccountRequest("admin"));
        postJson("/account/create", new CreateAccountRequest("user"));
        postJson("/billing/deposit", new DepositRequest("admin", ADMIN_MONEY_AMOUNT));
        objectMapper = new ObjectMapper();
    }

    @Test
    public void cannotCreateAnotherUserWithExistingLogin() throws IOException {
        WrappedResponse response = postObject("/account/create", new CreateAccountRequest("admin"));
        assertNotNull(response.getError());
    }

    @Test
    public void cannotWithdrawMoreThanAccountHas() throws IOException {
        WrappedResponse response = postObject("/billing/withdraw", new WithdrawRequest("admin", ADMIN_MONEY_AMOUNT + 1));
        assertNotNull(response.getError());
    }

    @Test
    public void cannotTransferMoreThanAccountHas() throws IOException {
        WrappedResponse response = postObject("/billing/transfer", new TransferRequest("admin", "user", ADMIN_MONEY_AMOUNT + 1));
        assertNotNull(response.getError());
    }

    @Test
    public void checkDeposit() throws IOException {
        WrappedResponse response = postObject("/account/create", new CreateAccountRequest("depositUser"));
        assertNull(response.getError());
        WrappedResponse<Account> accountResponse = postAccount("/billing/deposit", new DepositRequest("depositUser", 10));
        assertNull(response.getError());
        assertEquals(accountResponse.getData().getMoney(), 10);
    }

    @Test
    public void checkWithdraw() throws IOException {
        WrappedResponse response = postObject("/account/create", new CreateAccountRequest("withdrawUser"));
        assertNull(response.getError());
        postAccount("/billing/deposit", new DepositRequest("withdrawUser", 10));
        assertNull(response.getError());
        WrappedResponse<Account> accountResponse = postAccount("/billing/withdraw", new DepositRequest("withdrawUser", 5));
        assertNull(response.getError());
        assertEquals(accountResponse.getData().getMoney(), 5);
    }

    @Test
    public void checkTransfer() throws IOException {
        int AMOUNT_TO_TRANSFER = 10;

        WrappedResponse transferResponse = postObject("/billing/transfer", new TransferRequest("admin", "user", AMOUNT_TO_TRANSFER));
        assertNull(transferResponse.getError());

        WrappedResponse<Account> accountResponse = postAccount("/account/info", new GetAccountRequest("admin"));
        assertNull(accountResponse.getError());
        assertEquals(accountResponse.getData().getMoney(), ADMIN_MONEY_AMOUNT - AMOUNT_TO_TRANSFER);
        accountResponse = postAccount("/account/info", new GetAccountRequest("user"));
        assertNull(accountResponse.getError());
        assertEquals(accountResponse.getData().getMoney(), AMOUNT_TO_TRANSFER);

        transferResponse = postObject("/billing/transfer", new TransferRequest("user", "admin", AMOUNT_TO_TRANSFER));
        assertNull(transferResponse.getError());

        accountResponse = postAccount("/account/info", new GetAccountRequest("admin"));
        assertNull(accountResponse.getError());
        assertEquals(accountResponse.getData().getMoney(), ADMIN_MONEY_AMOUNT);
        accountResponse = postAccount("/account/info", new GetAccountRequest("user"));
        assertNull(accountResponse.getError());
        assertEquals(accountResponse.getData().getMoney(), 0);
    }

    @AfterClass
    public static void stopServers() throws Exception {
        Main.stop(h2Server, jettyServer);
    }

    private static WrappedResponse<Account> postAccount(String method, Object payload) throws IOException {
        String response = postJson(method, payload);

        return objectMapper.readValue(response, new TypeReference<WrappedResponse<Account>>() {});
    }

    private static WrappedResponse postObject(String method, Object payload) throws IOException {
        String response = postJson(method, payload);

        return objectMapper.readValue(response, WrappedResponse.class);
    }

    private static String postJson(String method, Object payload) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpClient client = HttpClientBuilder.create().build();

        HttpPost post = new HttpPost("http://localhost:" + JETTY_PORT + method);
        post.addHeader("content-type", MediaType.APPLICATION_JSON);
        post.setEntity(new StringEntity(objectMapper.writeValueAsString(payload)));

        HttpResponse httpResponse = client.execute(post);
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        if (statusCode != Response.Status.OK.getStatusCode()
                && statusCode != Response.Status.BAD_REQUEST.getStatusCode()) {
            throw new IllegalArgumentException("Cannot execute " + method + " with payload " + payload);
        }
        return readResponse(httpResponse);
    }

    private static String readResponse(HttpResponse httpResponse) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
        StringBuilder result = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        return result.toString();
    }
}
