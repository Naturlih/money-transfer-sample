package ru.svshubin.transfer.resource;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ru.svshubin.transfer.model.Account;
import ru.svshubin.transfer.model.request.billing.DepositRequest;
import ru.svshubin.transfer.model.request.billing.TransferRequest;
import ru.svshubin.transfer.model.request.billing.WithdrawRequest;
import ru.svshubin.transfer.model.response.TransferResponse;
import ru.svshubin.transfer.model.response.error.ErrorResponse;
import ru.svshubin.transfer.service.BillingService;

import javax.inject.Inject;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
@Path("billing")
@Api(value = "/billing", description = "Manages billing of accounts - deposit, withdraw and transfer")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BillingResource {
    @Inject
    private BillingService billingService;

    @Inject
    private Validator validator;

    @Path("/transfer")
    @POST
    @ApiOperation(value = "/transfer",
            notes = "Transfers money from one account to another",
            response = TransferResponse.class,
            responseContainer = "WrappedResponse")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Cannot find account by 'from' login", response = ErrorResponse.class,
                    responseContainer = "WrappedResponse"),
            @ApiResponse(code = 400, message = "Cannot find account by 'to' login", response = ErrorResponse.class,
                    responseContainer = "WrappedResponse"),
            @ApiResponse(code = 400, message = "'From' account doesn't has enough money to withdraw", response = ErrorResponse.class,
                    responseContainer = "WrappedResponse")
    })
    public Response transfer(TransferRequest transferRequest) {
        ResourceUtil.validate(transferRequest, validator);
        TransferResponse response = billingService.transfer(transferRequest);

        return Response.ok(response).build();
    }

    @Path("/deposit")
    @POST
    @ApiOperation(value = "/deposit",
            notes = "Deposit money to specified account",
            response = Account.class,
            responseContainer = "WrappedResponse")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Cannot find account by login", response = ErrorResponse.class,
                    responseContainer = "WrappedResponse")
    })
    public Response deposit(DepositRequest depositRequest) {
        ResourceUtil.validate(depositRequest, validator);
        Account account = billingService.deposit(depositRequest);

        return Response.ok(account).build();
    }

    @Path("/withdraw")
    @POST
    @ApiOperation(value = "/withdraw",
            notes = "Withdraw money to specified account",
            response = Account.class,
            responseContainer = "WrappedResponse")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Cannot find account by login", response = ErrorResponse.class,
                    responseContainer = "WrappedResponse"),
            @ApiResponse(code = 400, message = "Account doesn't has enough money to withdraw", response = ErrorResponse.class,
                    responseContainer = "WrappedResponse")
    })
    public Response withdraw(WithdrawRequest withdrawRequest) {
        ResourceUtil.validate(withdrawRequest, validator);
        Account account = billingService.withdraw(withdrawRequest);

        return Response.ok(account).build();
    }
}
