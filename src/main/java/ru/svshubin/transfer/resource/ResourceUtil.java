package ru.svshubin.transfer.resource;

import ru.svshubin.transfer.model.response.error.exception.RequestValidationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 19.03.2017
 */
public class ResourceUtil {
    public static <T> void validate(T request, Validator validator) {
        Set<ConstraintViolation<T>> violationSet = validator.validate(request);

        if (!violationSet.isEmpty()) {
            String violationMessage = violationSet.stream()
                    .map(ConstraintViolation::getMessage)
                    .collect(Collectors.joining("; "));

            throw new RequestValidationException(violationMessage);
        }
    }
}
