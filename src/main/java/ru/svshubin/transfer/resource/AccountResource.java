package ru.svshubin.transfer.resource;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import ru.svshubin.transfer.model.Account;
import ru.svshubin.transfer.model.response.error.ErrorResponse;
import ru.svshubin.transfer.model.response.error.ErrorResponseCode;
import ru.svshubin.transfer.model.response.error.exception.AccountWithLoginAlreadyExistsException;
import ru.svshubin.transfer.model.response.error.exception.CannotFindAccountByLoginException;
import ru.svshubin.transfer.model.request.account.CreateAccountRequest;
import ru.svshubin.transfer.model.request.account.GetAccountRequest;
import ru.svshubin.transfer.resource.interceptor.WrappedResponse;
import ru.svshubin.transfer.service.AccountService;

import javax.inject.Inject;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
@Path("account")
@Api(value = "/account", description = "Manages creation of accounts and retrieval of account info")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {
    @Inject
    private AccountService accountService;

    @Inject
    private Validator validator;

    @Path("/info")
    @POST
    @ApiOperation(value = "/info",
            notes = "Gets account info by login",
            response = Account.class,
            responseContainer = "WrappedResponse")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Cannot find account by login", response = ErrorResponse.class,
                    responseContainer = "WrappedResponse")
    })
    public Response getAccountInfo(GetAccountRequest getAccountRequest) throws CannotFindAccountByLoginException {
        ResourceUtil.validate(getAccountRequest, validator);

        Account account = accountService.getAccount(getAccountRequest);

        return Response.ok(account).build();
    }

    @Path("/create")
    @POST
    @ApiOperation(value = "/create",
            notes = "Creates account by login",
            response = Account.class,
            responseContainer = "WrappedResponse")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Account with specified login exists already", response = ErrorResponse.class,
                    responseContainer = "WrappedResponse")
    })
    public Response createAccount(CreateAccountRequest createAccountRequest) throws AccountWithLoginAlreadyExistsException {
        ResourceUtil.validate(createAccountRequest, validator);
        Account account = accountService.createAccount(createAccountRequest);

        return Response.ok(account).build();
    }
}
