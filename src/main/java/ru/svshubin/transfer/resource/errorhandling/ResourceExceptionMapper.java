package ru.svshubin.transfer.resource.errorhandling;

import ru.svshubin.transfer.model.response.error.exception.AbstractRequestException;
import ru.svshubin.transfer.model.response.error.ErrorResponse;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
@Provider
public class ResourceExceptionMapper implements ExceptionMapper<AbstractRequestException> {
    public Response toResponse(AbstractRequestException rre) {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorResponse(rre.getErrorCode().getId(), rre.getMessage()), null)
                .build();
    }
}
