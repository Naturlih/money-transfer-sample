package ru.svshubin.transfer.resource.interceptor;

import ru.svshubin.transfer.model.response.error.ErrorResponse;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import java.io.IOException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public class ResponseWrappingInterceptor implements WriterInterceptor {
    @Override
    public void aroundWriteTo(WriterInterceptorContext writerInterceptorContext)
            throws IOException, WebApplicationException {
        WrappedResponse wrappedResponse;
        Object payload = writerInterceptorContext.getEntity();
        if (payload instanceof ErrorResponse) {
            wrappedResponse = new WrappedResponse<>(((ErrorResponse) payload), null);
        } else {
            wrappedResponse = new WrappedResponse<>(null, payload);
        }
        writerInterceptorContext.setEntity(wrappedResponse);
        writerInterceptorContext.proceed();
    }
}
