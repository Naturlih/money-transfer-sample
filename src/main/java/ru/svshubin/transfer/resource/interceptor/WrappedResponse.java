package ru.svshubin.transfer.resource.interceptor;

import ru.svshubin.transfer.model.response.error.ErrorResponse;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public class WrappedResponse<T> {
    private ErrorResponse error;
    private T data;

    WrappedResponse() {
    }

    WrappedResponse(ErrorResponse error, T data) {
        this.error = error;
        this.data = data;
    }

    public ErrorResponse getError() {
        return error;
    }

    public T getData() {
        return data;
    }

    @Override
    public String toString() {
        return "WrappedResponse{" +
                "error=" + error +
                ", data=" + data +
                '}';
    }
}
