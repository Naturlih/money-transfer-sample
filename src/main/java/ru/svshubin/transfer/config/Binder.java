package ru.svshubin.transfer.config;

import org.glassfish.hk2.api.Factory;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.h2.jdbcx.JdbcDataSource;
import ru.svshubin.transfer.service.AccountService;
import ru.svshubin.transfer.service.AccountServiceImpl;
import ru.svshubin.transfer.service.BillingService;
import ru.svshubin.transfer.service.BillingServiceImpl;

import javax.inject.Singleton;
import javax.sql.DataSource;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public class Binder extends AbstractBinder {
    private static final String SERIALIZABLE_TRANSACTION_ISOLATION_LEVEL = ";LOCK_MODE=1";
    private static final String DONT_ERASE_DATABASE = ";DB_CLOSE_DELAY=-1";
    private static final String TABLE_LOCK_TIMEOUT = ";LOCK_TIMEOUT=30000";

    @Override
    protected void configure() {
        DataSource ds = initDatabase();
        bind(ds).to(DataSource.class);
        bind(AccountServiceImpl.class)
                .to(AccountService.class)
                .in(Singleton.class);
        bind(BillingServiceImpl.class)
                .to(BillingService.class)
                .in(Singleton.class);
        bindFactory(getValidatorFactory())
                .to(Validator.class);
    }

    private Factory<Validator> getValidatorFactory() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        return new Factory<Validator>() {
            @Override
            public Validator provide() {
                return validatorFactory.getValidator();
            }

            @Override
            public void dispose(Validator instance) {}
        };
    }

    private DataSource initDatabase() {
        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL("jdbc:h2:mem:db" + SERIALIZABLE_TRANSACTION_ISOLATION_LEVEL + DONT_ERASE_DATABASE + TABLE_LOCK_TIMEOUT);

        String databaseSchema = "CREATE TABLE account (" +
                "id LONG PRIMARY KEY AUTO_INCREMENT," +
                "login VARCHAR(30) UNIQUE NOT NULL," +
                "money LONG NOT NULL" +
                ")";

        try (Connection connection = ds.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(databaseSchema)) {
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return ds;
    }
}
