package ru.svshubin.transfer;

import ru.svshubin.transfer.config.Binder;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import ru.svshubin.transfer.resource.interceptor.ResponseWrappingInterceptor;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public class Main {
    public static void main(String[] args) throws Exception {
        org.h2.tools.Server h2Server = null;
        Server jettyServer = null;
        try {
            h2Server = startH2Database();
            jettyServer = startJetty(8080);

            jettyServer.join();
        } finally {
            stop(h2Server, jettyServer);
        }
    }

    public static org.h2.tools.Server startH2Database() throws SQLException {
        return org.h2.tools.Server.createTcpServer().start();
    }

    public static Server startJetty(int port) throws Exception {
        URI baseUri = UriBuilder.fromUri("http://localhost/").port(port).build();
        ResourceConfig config = new ResourceConfig();
        config.packages("ru/svshubin/transfer/resource");
        config.register(new Binder());
        config.register(ResponseWrappingInterceptor.class);
        Server jettyServer = JettyHttpContainerFactory.createServer(baseUri, config);
        jettyServer.start();

        return jettyServer;
    }

    public static void stop(org.h2.tools.Server h2Server, Server jettyServer) throws Exception {
        if (h2Server != null) {
            try {
                h2Server.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (jettyServer != null) {
            jettyServer.stop();
            jettyServer.destroy();
        }
    }
}
