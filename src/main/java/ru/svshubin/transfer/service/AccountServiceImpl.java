package ru.svshubin.transfer.service;

import ru.svshubin.transfer.model.Account;
import ru.svshubin.transfer.model.response.error.exception.AccountWithLoginAlreadyExistsException;
import ru.svshubin.transfer.model.response.error.exception.CannotFindAccountByLoginException;
import ru.svshubin.transfer.model.request.account.CreateAccountRequest;
import ru.svshubin.transfer.model.request.account.GetAccountRequest;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
@Service
public class AccountServiceImpl implements AccountService {
    @Inject
    private DataSource dataSource;

    @Override
    public Account createAccount(CreateAccountRequest createAccountRequest) throws AccountWithLoginAlreadyExistsException {
        String loginOfNewAccount = createAccountRequest.getLogin();
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            Account account = executeGetAccount(loginOfNewAccount, connection);
            if (account != null) {
                connection.rollback();

                throw new AccountWithLoginAlreadyExistsException(loginOfNewAccount);
            }
            executeCreateAccount(loginOfNewAccount, connection);
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try {
            return getAccount(new GetAccountRequest(loginOfNewAccount));
        } catch (CannotFindAccountByLoginException e) {
            throw new RuntimeException("Cannot find account by login " + loginOfNewAccount
                    + " after creation", e);
        }
    }

    @Override
    public Account getAccount(GetAccountRequest getAccountRequest) throws CannotFindAccountByLoginException {
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            Account account = executeGetAccount(getAccountRequest.getLogin(), connection);
            if (account == null) {
                connection.rollback();

                throw new CannotFindAccountByLoginException(getAccountRequest.getLogin());
            }
            connection.commit();

            return account;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Account executeGetAccount(String login, Connection conn) throws SQLException {
        //SELECT (id, login, money) wont work, rs.getString(1) will return "(1, asd, 0" instead of "1"
        try (PreparedStatement preparedStatement = conn.prepareStatement("SELECT * FROM account WHERE login = ?")) {
            preparedStatement.setString(1, login);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                boolean hasAccount = resultSet.next();
                if (!hasAccount) {
                    return null;
                }

                return new Account(resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getLong(3));
            }
        }
    }

    private void executeCreateAccount(String login, Connection conn) throws SQLException {
        try (PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO account (login, money) VALUES(?, ?)")) {
            preparedStatement.setString(1, login);
            preparedStatement.setLong(2, 0);

            preparedStatement.executeUpdate();
        }
    }
}
