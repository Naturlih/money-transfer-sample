package ru.svshubin.transfer.service;

import ru.svshubin.transfer.model.Account;
import ru.svshubin.transfer.model.response.error.exception.AccountWithLoginAlreadyExistsException;
import ru.svshubin.transfer.model.response.error.exception.CannotFindAccountByLoginException;
import ru.svshubin.transfer.model.request.account.CreateAccountRequest;
import ru.svshubin.transfer.model.request.account.GetAccountRequest;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public interface AccountService {
    Account createAccount(CreateAccountRequest createAccountRequest) throws AccountWithLoginAlreadyExistsException;

    Account getAccount(GetAccountRequest getAccountRequest) throws CannotFindAccountByLoginException;
}
