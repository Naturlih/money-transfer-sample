package ru.svshubin.transfer.service;

import ru.svshubin.transfer.model.Account;
import ru.svshubin.transfer.model.request.billing.DepositRequest;
import ru.svshubin.transfer.model.request.billing.TransferRequest;
import ru.svshubin.transfer.model.request.billing.WithdrawRequest;
import ru.svshubin.transfer.model.response.TransferResponse;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public interface BillingService {
    TransferResponse transfer(TransferRequest transferRequest);

    Account deposit(DepositRequest depositRequest);

    Account withdraw(WithdrawRequest withdrawRequest);
}
