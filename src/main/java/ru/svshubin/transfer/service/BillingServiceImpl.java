package ru.svshubin.transfer.service;

import ru.svshubin.transfer.model.Account;
import ru.svshubin.transfer.model.response.error.exception.CannotFindAccountByLoginException;
import ru.svshubin.transfer.model.response.error.exception.NotEnoughMoneyException;
import ru.svshubin.transfer.model.request.account.GetAccountRequest;
import ru.svshubin.transfer.model.request.billing.DepositRequest;
import ru.svshubin.transfer.model.request.billing.TransferRequest;
import ru.svshubin.transfer.model.request.billing.WithdrawRequest;
import ru.svshubin.transfer.model.response.TransferResponse;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public class BillingServiceImpl implements BillingService {
    private static final String DEPOSIT_ACCOUNT_SQL = "UPDATE account SET money = money + ? where login = ?";
    private static final String WITHDRAW_ACCOUNT_SQL = "UPDATE account SET money = money - ? where login = ?";

    @Inject
    private DataSource dataSource;

    @Inject
    private AccountService accountService;

    @Override
    public TransferResponse transfer(TransferRequest transferRequest) {
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            validateExistenceOfAccount(transferRequest.getLoginFrom());
            validateExistenceOfAccount(transferRequest.getLoginTo());
            executeWithdraw(transferRequest.getLoginFrom(), transferRequest.getAmount(), connection);
            executeDeposit(transferRequest.getLoginTo(), transferRequest.getAmount(), connection);
            connection.commit();

            Account accountFrom = accountService.getAccount(new GetAccountRequest(transferRequest.getLoginFrom()));
            Account accountTo = accountService.getAccount(new GetAccountRequest(transferRequest.getLoginTo()));

            return new TransferResponse(accountFrom, accountTo);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Account deposit(DepositRequest depositRequest) {
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            validateExistenceOfAccount(depositRequest.getLogin());
            executeDeposit(depositRequest.getLogin(), depositRequest.getAmount(), connection);
            connection.commit();

            return accountService.getAccount(new GetAccountRequest(depositRequest.getLogin()));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Account withdraw(WithdrawRequest withdrawRequest) {
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            validateExistenceOfAccount(withdrawRequest.getLogin());
            executeWithdraw(withdrawRequest.getLogin(), withdrawRequest.getAmount(), connection);
            connection.commit();

            return accountService.getAccount(new GetAccountRequest(withdrawRequest.getLogin()));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void validateExistenceOfAccount(String login) {
        if (accountService.getAccount(new GetAccountRequest(login)) == null) {
            throw new CannotFindAccountByLoginException(login);
        }
    }

    private void executeDeposit(String login, long amount, Connection conn) throws SQLException {
        try (PreparedStatement depositStatement = conn.prepareStatement(DEPOSIT_ACCOUNT_SQL)) {
            depositStatement.setLong(1, amount);
            depositStatement.setString(2, login);

            depositStatement.execute();
        }
    }

    private void executeWithdraw(String login, long amount, Connection conn) throws SQLException {
        Account account = accountService.getAccount(new GetAccountRequest(login));
        if (account.getMoney() < amount) {
            conn.rollback();
            throw new NotEnoughMoneyException(account, amount);
        }

        try (PreparedStatement withdrawStatement = conn.prepareStatement(WITHDRAW_ACCOUNT_SQL)) {
            withdrawStatement.setLong(1, amount);
            withdrawStatement.setString(2, login);

            withdrawStatement.execute();
        }
    }
}
