package ru.svshubin.transfer.model.response.error.exception;

import ru.svshubin.transfer.model.response.error.ErrorResponseCode;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public class AccountWithLoginAlreadyExistsException extends AbstractRequestException {
    public AccountWithLoginAlreadyExistsException(String login) {
        super("Account with login '" + login + "' already exists.");
    }

    public ErrorResponseCode getErrorCode() {
        return ErrorResponseCode.ACCOUNT_ALREADY_EXIST;
    }
}
