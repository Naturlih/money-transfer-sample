package ru.svshubin.transfer.model.response.error.exception;

import ru.svshubin.transfer.model.Account;
import ru.svshubin.transfer.model.response.error.ErrorResponseCode;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public class NotEnoughMoneyException extends AbstractRequestException {

    public NotEnoughMoneyException(Account account, long amountToWithdraw) {
        super("Not enough money on account '" + account.getLogin() + "' to withdraw " + amountToWithdraw);
    }

    @Override
    public ErrorResponseCode getErrorCode() {
        return ErrorResponseCode.NOT_ENOUGH_MONEY;
    }
}
