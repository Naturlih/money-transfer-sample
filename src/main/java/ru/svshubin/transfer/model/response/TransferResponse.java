package ru.svshubin.transfer.model.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import ru.svshubin.transfer.model.Account;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 19.03.2017
 */
@ApiModel(value = "TransferResponse", description = "Transfer response")
public class TransferResponse {
    @ApiModelProperty(value = "Account of withdraw", required = true)
    private Account from;
    @ApiModelProperty(value = "Account of deposit", required = true)
    private Account to;

    public TransferResponse(Account from, Account to) {
        this.from = from;
        this.to = to;
    }

    public Account getFrom() {
        return from;
    }

    public Account getTo() {
        return to;
    }
}
