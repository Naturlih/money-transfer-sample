package ru.svshubin.transfer.model.response.error.exception;

import ru.svshubin.transfer.model.response.error.ErrorResponseCode;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public abstract class AbstractRequestException extends RuntimeException {
    public AbstractRequestException() {
    }

    public AbstractRequestException(String message) {
        super(message);
    }

    public AbstractRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractRequestException(Throwable cause) {
        super(cause);
    }

    public abstract ErrorResponseCode getErrorCode();
}
