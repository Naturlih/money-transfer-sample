package ru.svshubin.transfer.model.response.error;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public enum ErrorResponseCode {
    REQUEST_VALIDATION_FAILED(0),
    ACCOUNT_ALREADY_EXIST(1),
    CANNOT_FIND_ACCOUNT(2),
    NOT_ENOUGH_MONEY(3),
    ;

    private final int id;

    ErrorResponseCode(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
