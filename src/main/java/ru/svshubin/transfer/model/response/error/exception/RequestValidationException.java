package ru.svshubin.transfer.model.response.error.exception;

import ru.svshubin.transfer.model.response.error.ErrorResponseCode;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 19.03.2017
 */
public class RequestValidationException extends ru.svshubin.transfer.model.response.error.exception.AbstractRequestException {
    public RequestValidationException(String message) {
        super(message);
    }

    @Override
    public ErrorResponseCode getErrorCode() {
        return ErrorResponseCode.REQUEST_VALIDATION_FAILED;
    }
}
