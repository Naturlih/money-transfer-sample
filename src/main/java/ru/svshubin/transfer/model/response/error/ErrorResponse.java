package ru.svshubin.transfer.model.response.error;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public class ErrorResponse {
    private int errorCode;
    private String errorMessage;

    public ErrorResponse() {
    }

    public ErrorResponse(int errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "errorCode=" + errorCode +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
