package ru.svshubin.transfer.model.response.error.exception;

import ru.svshubin.transfer.model.response.error.ErrorResponseCode;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
public class CannotFindAccountByLoginException extends AbstractRequestException {
    public CannotFindAccountByLoginException(String login) {
        super("Cannot find account by login '" + login + "'");
    }

    @Override
    public ErrorResponseCode getErrorCode() {
        return ErrorResponseCode.CANNOT_FIND_ACCOUNT;
    }
}
