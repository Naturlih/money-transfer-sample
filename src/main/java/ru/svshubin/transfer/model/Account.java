package ru.svshubin.transfer.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
@ApiModel(value = "Account", description = "Account representation")
public class Account {
    @ApiModelProperty(value = "Unique id of account", required = true)
    private long id;
    @ApiModelProperty(value = "Unique login of account, not empty", required = true)
    private String login;
    @ApiModelProperty(value = "Money on account, not negative", required = true)
    private long money;

    public Account() {
    }

    public Account(long id, String login, long money) {
        this.id = id;
        this.login = login;
        this.money = money;
    }

    public Account(String login) {
        this.login = login;
    }

    public long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public long getMoney() {
        return money;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        return login != null ? login.equals(account.login) : account.login == null;
    }

    @Override
    public int hashCode() {
        return login != null ? login.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", money=" + money +
                '}';
    }
}
