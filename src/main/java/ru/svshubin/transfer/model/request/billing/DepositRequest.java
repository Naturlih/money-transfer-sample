package ru.svshubin.transfer.model.request.billing;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
@ApiModel(value = "DepositRequest", description = "Deposit request")
public class DepositRequest {
    @NotEmpty
    @ApiModelProperty(value = "Login of account to deposit to, not empty", required = true)
    private String login;
    @Min(0)
    @ApiModelProperty(value = "Amount to deposit, not negative", required = true)
    private long amount;

    public DepositRequest() {
    }

    public DepositRequest(String login, long amount) {
        this.login = login;
        this.amount = amount;
    }

    public String getLogin() {
        return login;
    }

    public long getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "DepositRequest{" +
                "login='" + login + '\'' +
                ", amount=" + amount +
                '}';
    }
}
