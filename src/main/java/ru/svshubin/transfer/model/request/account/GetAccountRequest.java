package ru.svshubin.transfer.model.request.account;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
@ApiModel(value = "GetAccountRequest", description = "Get account request")
public class GetAccountRequest {
    @NotEmpty
    @ApiModelProperty(value = "Login of account, not empty", required = true)
    private String login;

    public GetAccountRequest() {
    }

    public GetAccountRequest(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    @Override
    public String toString() {
        return "GetAccountRequest{" +
                "login='" + login + '\'' +
                '}';
    }
}
