package ru.svshubin.transfer.model.request.billing;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;

/**
 * @author Sergey Shubin (sergey.vl.shubin@gmail.com) on 18.03.2017
 */
@ApiModel(value = "TransferRequest", description = "Transfer request")
public class TransferRequest {
    @NotEmpty
    @ApiModelProperty(value = "Login of account to withdraw from, not empty", required = true)
    private String loginFrom;
    @NotEmpty
    @ApiModelProperty(value = "Login of account to deposit to, not empty", required = true)
    private String loginTo;
    @Min(0)
    @ApiModelProperty(value = "Amount to transfer, not negative", required = true)
    private long amount;

    public TransferRequest() {

    }

    public TransferRequest(String loginFrom, String loginTo, long amount) {
        this.loginFrom = loginFrom;
        this.loginTo = loginTo;
        this.amount = amount;
    }

    public String getLoginFrom() {
        return loginFrom;
    }

    public String getLoginTo() {
        return loginTo;
    }

    public long getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "TransferRequest{" +
                "loginFrom='" + loginFrom + '\'' +
                ", loginTo='" + loginTo + '\'' +
                ", amount=" + amount +
                '}';
    }
}
